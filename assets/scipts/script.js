const carIDInput = document.getElementById('id');
const carNameInput = document.getElementById('name');
const carColorInput = document.getElementById('color');


window.addEventListener('load', () => {
    appendAddBtn();
    createDataOnStorage();
    let dataArray = getArrayFromStorage();
    let Http = new XMLHttpRequest();
    Http.open("GET", "https://hy8dazex17.execute-api.us-east-2.amazonaws.com/cars");
    
    Http.send();
    Http.onreadystatechange = (e) => {
        if (e.currentTarget.status == 200 && e.currentTarget.readyState == 4) {
            resp = Http.responseText
            resp = JSON.parse(resp)
            dataArray = resp.Items
            dataArray.forEach((obj) => {
                appendTableData(obj.id, obj.name, obj.color);
            });
        }
    }

});

function editBtn() {
    const tdObj = this.parentElement.parentElement;
    const idObj = tdObj.querySelector('.id-td')
    const nameObj = tdObj.querySelector('.name-td');
    const colorObj = tdObj.querySelector('.color-td');
    carIDInput.value = idObj.textContent;
    carNameInput.value = nameObj.textContent;
    carColorInput.value = colorObj.textContent;
    appendEditBtns.call(this);
}

function cancelFunc() {
    clearUsrInput();
    appendAddBtn();
}

function editFunc() {
    const TRData = getTRData.call(this);
    const idField = TRData[0];
    const nameField = TRData[1];
    const colorField = TRData[2];
    if (inputCheck(carIDInput.value,carNameInput.value, carColorInput.value)) return;
    updateValueOnStorage(
        idField.textContent,
        nameField.textContent,
        colorField.textContent,
        carIDInput.value,
        carNameInput.value,
        carColorInput.value,
    );
    idField.textContent = carIDInput.value;
    nameField.textContent = carNameInput.value;
    colorField.textContent = carColorInput.value;
    let Http = new XMLHttpRequest();
    Http.open("PUT", "https://hy8dazex17.execute-api.us-east-2.amazonaws.com/cars");
    Http.send(JSON.stringify({ "id":carIDInput.value,
                              "name": carNameInput.value,
                              "color": carColorInput.value }));
    cancelFunc();
}

function appendEditBtns() {
    const actionDiv = removeActionBtns();
    const updateBtnTemp = document.getElementsByTagName('template')[1];
    const btnsClone = updateBtnTemp.content.cloneNode(true);
    const updateBtn = btnsClone.querySelector('.btn-outline-primary');
    const cancelBtn = btnsClone.querySelector('.btn-outline-secondary');
    updateBtn.addEventListener('click', editFunc.bind(this));
    cancelBtn.addEventListener('click', cancelFunc);
    actionDiv.appendChild(btnsClone);
}

function removeActionBtns() {
    const btnDiv = document.querySelector('.action-btns');
    const btns = btnDiv.querySelectorAll('button');
    btns.forEach((item) => item.remove());
    return btnDiv;
}

function appendAddBtn() {
    const actionDiv = removeActionBtns();
    const addBtnTemp = document.getElementsByTagName('template')[2];
    const btnsClone = addBtnTemp.content.cloneNode(true);
    const addBtn = btnsClone.querySelector('.btn-outline-success');
    addBtn.addEventListener('click', defaultAddFunc);
    actionDiv.appendChild(btnsClone);
}

function deleteBtn() {
    const TRData = getTRData.call(this);
    const dataArray = getArrayFromStorage();
    const index = dataArray.findIndex((obj) => {
        return (
            obj.id == TRData[0].textContent &&
            obj.name == +TRData[1].textContent &&
            obj.color == +TRData[2].textContent
        );
    });
    dataArray.splice(index, 1);
    localStorage.setItem('data', JSON.stringify(dataArray));
    let Http = new XMLHttpRequest();
    Http.open("DELETE", "https://hy8dazex17.execute-api.us-east-2.amazonaws.com/cars/"+TRData[0].textContent);
    Http.send();
    this.parentElement.parentElement.remove();
    
}

function defaultAddFunc() {
    let Http = new XMLHttpRequest();
    Http.open("PUT", "https://hy8dazex17.execute-api.us-east-2.amazonaws.com/cars");
    Http.send(JSON.stringify({ "id":carIDInput.value,
                              "name": carNameInput.value,
                              "color": carColorInput.value }));
    
    appendTableData(carIDInput.value,carNameInput.value, carColorInput.value);
    clearUsrInput();
}

function clearUsrInput() {
    carIDInput.value = '';
    carNameInput.value = '';
    carColorInput.value = '';
}

function appendTableData(id,name,color) {
    if (inputCheck(id,name,color)) return;
    const templateClone = retrieveTemplate(id,name,color);
    addToLocalStorage(id,name,color);
    const table = document.querySelector('.tbody-content');
    table.appendChild(templateClone);
}

function inputCheck(id,name,color) {
    if (!id && !name && !color) {
        alert('You need to fill all fields to add a new car!');
        cancelFunc();
        return true;
    }
//    if (!name || name.match(/[0-9]/)) {
//        cancelFunc();
//        alert('Invalid name!');
//        return true;
//    }
//    if (!color || color.match(/[0-9]/)) {
//        cancelFunc();
//        alert('Invalid color!');
//        return true;
//    }
   
}

function addToLocalStorage(id,name, color) {
    if (checkIfExists(id, name,color)) return;
    const dataArray = getArrayFromStorage();
    const newEntry = {
        id: id,
        name: name,
        color: color,
    };
    dataArray.push(newEntry);
    localStorage.setItem('data', JSON.stringify(dataArray));
}

function retrieveTemplate(id,name,color) {
    if (inputCheck(id,name,color)) return;
    const temp = document.getElementsByTagName('template')[0];
    const tempClone = temp.content.cloneNode(true);
    const tempIDTr = tempClone.querySelector('.id-td');
    const tempNameTr = tempClone.querySelector('.name-td');
    const tempColorTr = tempClone.querySelector('.color-td');
    const delBtn = tempClone.querySelector('.btn-outline-secondary');
    const editButn = tempClone.querySelector('.btn-outline-primary');
    delBtn.addEventListener('click', deleteBtn);
    editButn.addEventListener('click', editBtn);
        
    tempIDTr.textContent = id;
    tempNameTr.textContent = name;
    tempColorTr.textContent = color;
    return tempClone;
}

function checkIfExists(id,name,color) {
    const dataArray = getArrayFromStorage();
    const result = dataArray.find((object) => {
        return object.id == id;
    });
    return result;
}

function createDataOnStorage() {
    if (!localStorage.getItem('data')) {
        localStorage.setItem('data', JSON.stringify([]));
    }
}

function getTRData() {
    const parentTr = this.parentElement.parentElement;
    const idField = parentTr.querySelector('.id-td');
    const nameField = parentTr.querySelector('.name-td');
    const colorField = parentTr.querySelector('.color-td');
    return [idField, nameField, colorField];
}

function updateValueOnStorage(id,name, color, newID, newName, newColor) {
    const dataArray = getArrayFromStorage();
    const index = dataArray.findIndex((obj) => {
        return obj.id == id
    });
    dataArray[index].id = newID;
    dataArray[index].name = newName;
    dataArray[index].color = newColor;
    localStorage.setItem('data', JSON.stringify(dataArray));
}

function getArrayFromStorage() {
    const localStorageString = localStorage.getItem('data');
    const localStorageData = JSON.parse(localStorageString);
    return localStorageData;
}
